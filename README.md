# Issue Templates

A repository to store issue templates used in boards (i.e. Jira, Gitlab, etc) describing the TITLE format to use, DESCRIPTION format to use, and USAGE instructions for the issue type itself.

Currently this includes the following issue types:

- [User Stories](user-story.md)
- [Bugs](bug.md)
- [Spike Tasks](spike-task.md)
- [Work Tasks](work-task.md)
- [Epics](epic.md)