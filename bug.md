#### TITLE
<hr/>
short_descr_of_where_and_what_is_occuring
<hr/>

#### DESCRIPTION
<hr/>

description_of_where_and_what

**Current Behavior**

**Given** a_certain_application_state \
**When** a_user_takes_an_action \
**Then** bad_application_state

**Expected Behavior**

**Given** a_certain_application_state \
**When** a_user_takes_an_action \
**Then** good_application_state

**Notes**

bug_notes

<hr/>


#### USAGE
<hr/>

Bugs describe user value or behavoir that is not working as expected.  As such they are tasks to address issues that arise within the network, not to add new features and value to it.  They are also the most dynamic task type given that they are likely to appear mid-sprint.